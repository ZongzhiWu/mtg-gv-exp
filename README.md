### Supporting data for  

## Giant virus diversity and host interactions through global metagenomics

_Frederik Schulz, Simon Roux, David Paez-Espino, Sean Jungbluth, David Walsh, Vincent J. Denef, Katherine D. McMahon, Konstantinos T. Konstantinidis, Emiley A. Eloe-Fadrosh, Nikos Kyrpides, Tanja Woyke_

Direct download of the GVMAGs is also possible at https://figshare.com/s/14788165283d65466732

This repository contains all GVMAGs, multiple sequence alignments, phylogenetic tress, orthogroups, models for NCLDV major capsid proteins and the NCLDV detector described in the paper.

Please contact Frederik Schulz (fschulz@lbl.gov) for any questions regarding the data.

### References

https://doi.org/10.1038/s41586-020-1957-x
